package org.example;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class App
{
    private static EntityManager entityManager;
    static final String[] NAMES = {"Burger", "Pizza", "Soup", "Salad Caesar", "Borsch"};
    static final Random RANDOM = new Random();

    public static void main(String[] args)
    {
        try (Scanner scanner = new Scanner(System.in)) {
            EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
            entityManager = entityManagerFactory.createEntityManager();

            try {
                while (true) {
                    System.out.println("1: Add product");
                    System.out.println("2: Add random products");
                    System.out.println("3: View products");
                    System.out.println("4: Select products under 1 kg");
                    System.out.print("-> ");

                    String selection = scanner.nextLine();
                    switch (selection) {
                        case "1" -> addProduct(scanner);
                        case "2" -> insertRandomProducts(scanner);
                        case "3" -> viewProducts(scanner);
                        case "4" -> getProductsPackUpToOneKilogram();
                        default -> {
                            return;
                        }
                    }
                }
            } finally {
                entityManager.close();
                entityManagerFactory.close();
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    private static void addProduct(Scanner scanner) {
        System.out.print("Enter product name: ");
        String name = scanner.nextLine();
        System.out.print("Enter product price: ");
        String sPrice = scanner.nextLine();
        double price = Double.parseDouble(sPrice);
        System.out.println("Enter product weight (g): ");
        String sWeight = scanner.nextLine();
        int weight = Integer.parseInt(sWeight);
        System.out.println("Has this product 10% discount (y - yes, n - no)?");
        String discountAnswer = scanner.nextLine();

        entityManager.getTransaction().begin();
        try {
            Product product = new Product(name, price, weight, discountAnswer.equals("y"));
            entityManager.persist(product);
            entityManager.getTransaction().commit();
            System.out.println("Product (ID: " + product.getId() + " successfully added.");
        } catch (Exception exception) {
            entityManager.getTransaction().rollback();
        }
    }

    private static void insertRandomProducts(Scanner scanner) {
        System.out.print("Enter products count: ");
        String sCount = scanner.nextLine();
        int count = Integer.parseInt(sCount);

        entityManager.getTransaction().begin();
        try {
            for (int i = 0; i < count; i++) {
                double price = getRandomDouble(10, 50);
                Product product = new Product(randomName(), price, RANDOM.nextInt(10, 1500), price > 30);
                entityManager.persist(product);
            }
            entityManager.getTransaction().commit();
        } catch (Exception exception) {
            entityManager.getTransaction().rollback();
        }
    }

    private static void viewProducts(Scanner scanner) {
        while (true) {
            System.out.println("1: All products");
            System.out.println("2: Filter");
            System.out.println("------------------");
            System.out.println("0: Back");
            System.out.print("-> ");

            String selection = scanner.nextLine();
            switch (selection) {
                case "1" -> viewAllProducts();
                case "2" -> viewFilteredProducts(scanner);
                default -> {
                    return;
                }
            }
        }
    }

    private static void viewAllProducts() {
        Query query = entityManager.createQuery("SELECT c FROM Product c", Product.class);
        List<Product> list = (List<Product>) query.getResultList();
        for (Product product : list) {
            System.out.println(product);
        }
    }

    private static void viewFilteredProducts(Scanner scanner) {
        System.out.print("Enter minimum price: ");
        String sMinPrice = scanner.nextLine();
        double minPrice = Double.parseDouble(sMinPrice);
        System.out.print("Enter maximum price: ");
        String sMaxPrice = scanner.nextLine();
        double maxPrice = Double.parseDouble(sMaxPrice);
        System.out.print("With discount (y - yes, n - no, a - all): ");
        String sDiscount = scanner.nextLine();

        StringBuilder stringBuilder = new StringBuilder("SELECT c FROM Product c WHERE ");
        if (!"a".equals(sDiscount)) {
            stringBuilder.append("c.isDiscountAvailable = :discount AND ");
        }
        stringBuilder.append("c.price BETWEEN :min AND :max");

        Query query = entityManager.createQuery(stringBuilder.toString(), Product.class);
        if (!"a".equals(sDiscount)) {
            query.setParameter("discount", "y".equals(sDiscount));
        }

        query.setParameter("min", minPrice);
        query.setParameter("max", maxPrice);
        List<Product> list = (List<Product>) query.getResultList();
        for (Product product : list) {
            System.out.println(product);
        }
    }

    private static String randomName() {
        return NAMES[RANDOM.nextInt(NAMES.length)];
    }

    private static double getRandomDouble(int min, int max) {
        return min + (max - min) * RANDOM.nextDouble();
    }

    private static void getProductsPackUpToOneKilogram() {
        int totalWeight = 0;
        List<Product> products = getProductsPackUpToWeight(new ArrayList<>(), 0, 1000, 0);
        for (Product product : products) {
            System.out.println(product);
            totalWeight += product.getWeight();
        }

        System.out.println("Total weight: " + totalWeight);
    }

    private static List<Product> getProductsPackUpToWeight(List<Product> selected, int iteration, int neededWeight, int currentWeight) {
        Query query = entityManager.createQuery("SELECT c FROM Product c WHERE weight < :maxWeight ORDER BY weight", Product.class);
        query.setParameter("maxWeight", neededWeight);
        query.setFirstResult(iteration * 5);
        query.setMaxResults(5);
        List<Product> list = (List<Product>) query.getResultList();
        if (list.isEmpty()) {
            return selected;
        }

        for (Product product : list) {
            if (currentWeight + product.getWeight() >= neededWeight) {
                return selected;
            }

            currentWeight += product.getWeight();
            selected.add(product);
        }

        return getProductsPackUpToWeight(selected, iteration + 1, neededWeight, currentWeight);
    }
}
