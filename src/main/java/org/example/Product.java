package org.example;

import javax.persistence.*;

@Entity
@Table(name = "Products")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private double price;

    @Column(nullable = false)
    private int weight;

    @Column(name = "is_discount_available")
    private boolean isDiscountAvailable;

    public Product() {
        super();
    }

    public Product(String name, double price, int weight, boolean isDiscountAvailable) {
        this.name = name;
        this.price = price;
        this.weight = weight;
        this.isDiscountAvailable = isDiscountAvailable;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public boolean isDiscountAvailable() {
        return isDiscountAvailable;
    }

    public void setDiscountAvailable(boolean discountAvailable) {
        isDiscountAvailable = discountAvailable;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", weight=" + weight +
                ", isDiscountAvailable=" + isDiscountAvailable +
                '}';
    }
}
